
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Ricardo
 */
public class Prestamo implements Serializable{
    private int id_prestamo;
    private int id_miembro;
    private Date fecha_prestamo;
    private Date fecha_devolucion;
    private String observaciones;

    
    
    public Prestamo() {
    }

    public Prestamo(int id_prestamo, int id_miembro, Date fecha_prestamo, Date fecha_devolucion, String observaciones) {
        this.id_prestamo = id_prestamo;
        this.id_miembro = id_miembro;
        this.fecha_prestamo = fecha_prestamo;
        this.fecha_devolucion = fecha_devolucion;
        this.observaciones = observaciones;
    }
    
    

    public int getId_prestamo() {
        return id_prestamo;
    }

    public void setId_prestamo(int id_prestamo) {
        this.id_prestamo = id_prestamo;
    }

    public int getId_miembro() {
        return id_miembro;
    }

    public void setId_miembro(int id_miembro) {
        this.id_miembro = id_miembro;
    }

    public Date getFecha_prestamo() {
        return fecha_prestamo;
    }

    public void setFecha_prestamo(Date fecha_prestamo) {
        this.fecha_prestamo = fecha_prestamo;
    }

    public Date getFecha_devolucion() {
        return fecha_devolucion;
    }

    public void setFecha_devolucion(Date fecha_devolucion) {
        this.fecha_devolucion = fecha_devolucion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public String toString() {
        return "prestamo{" + "id_prestamo=" + id_prestamo + ", id_miembro=" + id_miembro + ", fecha_prestamo=" + fecha_prestamo + ", fecha_devolucion=" + fecha_devolucion + ", observaciones=" + observaciones + '}';
    }
    
    
}
