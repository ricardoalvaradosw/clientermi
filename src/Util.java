
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JTextField;

public class Util {

    public static Map<String, String> leerArchivo(String nombreArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        Map<String, String> parametros = new HashMap<String, String>();
        String[] cadena = null;
        try {
            archivo = new File(nombreArchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            String linea;
            while ((linea = br.readLine()) != null) {
                cadena = new String[2];
                cadena = linea.split(":");
                parametros.put(cadena[0], cadena[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return parametros;
    }

    public static void guardarConfiguracionServidorEnArchivo(String nombreArchivo, Map<String, String> parametros) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            borrarContenidoArchivo(nombreArchivo);
            fichero = new FileWriter(nombreArchivo);
            pw = new PrintWriter(fichero);
            pw.println("host:" + parametros.get("host"));
            pw.println("puerto:" + parametros.get("puerto"));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

  

    public static void borrarContenidoArchivo(String nombreArchivo) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(nombreArchivo));
            bw.write("");
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static boolean validarCadenas(JTextField... cadenas){
        for(JTextField txt : cadenas){
            if(txt.getText().toString().length()==0){
                return false;
            }
        }
        return true;
    }
}
