
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

public class BuscarLibro extends javax.swing.JDialog {

    private JFrame quienLlama;
    AddLibroIntf libroRMI = null;
    private Map<String, String> parametros;
    private JDialog quienLlamaD;

    public BuscarLibro(JFrame quienLlama, boolean modal) {
        super(quienLlama, modal);
        initComponents();
        this.quienLlama = quienLlama;
        postBuild();

    }

    public BuscarLibro(JDialog quienLlama, boolean modal) {
        super(quienLlama, modal);
        initComponents();
        this.quienLlamaD = quienLlama;
        postBuild();

    }

    public void postBuild() {
        parametros = Util.leerArchivo("parametros.txt");
        try {
            Registry registry = LocateRegistry.getRegistry(parametros.get("host"), Integer.parseInt(parametros.get("puerto")));
            libroRMI = (AddLibroIntf) registry.lookup("libroRMI");
        } catch (Exception e) {
            e.printStackTrace();
        }
        listarLibros(true);
    }

    public void listarLibros(Boolean todos) {
        List<Libro> libros = null;
        String nombre = "", categoria = "", autor = "";
        Libro libro = new Libro();
        nombre = txtNombreLibro.getText();
        categoria = txtCategoriaLibro.getText();
        autor = txtAutorLibro.getText();
        if (todos) {
            try {
                libros = libroRMI.findAll();
            } catch (RemoteException ex) {
                Logger.getLogger(BuscarLibro.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {

            libro.setNombre(nombre);
            libro.setGenero(categoria);
            libro.setAutor(autor);
            try {
                libros = libroRMI.findAllByQuery(libro);
            } catch (RemoteException ex) {
                Logger.getLogger(BuscarLibro.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        DefaultTableModel model = (DefaultTableModel) tbRegistrarLibro.getModel();
        model.setRowCount(0);
        for (Libro l : libros) {
            Object[] rowdata = {l.getId_libro(), l.getIsbn(), l.getNombre(), l.getAutor(), l.getGenero(), l.getEditorial(), l.getNumpag(), l.getAñoedicion(), l.getStock()};
            model.addRow(rowdata);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombreLibro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtAutorLibro = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtCategoriaLibro = new javax.swing.JTextField();
        rbTodos = new javax.swing.JRadioButton();
        btnBuscar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbRegistrarLibro = new javax.swing.JTable();
        btnSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("BUSCAR LIBROS");

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Opciones de Búsqueda\n", 1, 0, new java.awt.Font("Calibri", 0, 12))); // NOI18N

        jLabel1.setText("Nombre");

        txtNombreLibro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNombreLibroKeyPressed(evt);
            }
        });

        jLabel2.setText("Autor");

        txtAutorLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAutorLibroActionPerformed(evt);
            }
        });

        jLabel3.setText("Categoria");

        txtCategoriaLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCategoriaLibroActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbTodos);
        rbTodos.setText("Todos");

        btnBuscar.setBackground(new java.awt.Color(255, 102, 102));
        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNombreLibro, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAutorLibro, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCategoriaLibro, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(rbTodos)
                .addGap(18, 18, 18)
                .addComponent(btnBuscar)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNombreLibro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtAutorLibro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtCategoriaLibro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rbTodos)
                    .addComponent(btnBuscar))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(204, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "REGISTRO DE LIBROS", 2, 0, new java.awt.Font("Calibri", 1, 14))); // NOI18N

        tbRegistrarLibro.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "ISBN", "Nombre", "Autor", "Categoria", "Editorial", "#Pag", "Año de Edicion", "Stock"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbRegistrarLibro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbRegistrarLibroMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbRegistrarLibro);
        tbRegistrarLibro.getColumnModel().getColumn(0).setPreferredWidth(10);
        tbRegistrarLibro.getColumnModel().getColumn(1).setPreferredWidth(15);
        tbRegistrarLibro.getColumnModel().getColumn(2).setPreferredWidth(100);
        tbRegistrarLibro.getColumnModel().getColumn(3).setPreferredWidth(90);
        tbRegistrarLibro.getColumnModel().getColumn(4).setPreferredWidth(80);
        tbRegistrarLibro.getColumnModel().getColumn(5).setPreferredWidth(80);
        tbRegistrarLibro.getColumnModel().getColumn(6).setPreferredWidth(25);
        tbRegistrarLibro.getColumnModel().getColumn(7).setPreferredWidth(30);
        tbRegistrarLibro.getColumnModel().getColumn(8).setPreferredWidth(50);

        btnSalir.setBackground(new java.awt.Color(255, 102, 102));
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnSalir.setText("SALIR");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(346, 346, 346)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalir)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtAutorLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAutorLibroActionPerformed
        listarLibros(false);
    }//GEN-LAST:event_txtAutorLibroActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        if (rbTodos.isSelected()) {
            listarLibros(true);
            txtNombreLibro.setText("");
            txtAutorLibro.setText("");
            txtCategoriaLibro.setText("");
        } else {
            listarLibros(false);
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void txtNombreLibroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreLibroKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            listarLibros(false);
        }
    }//GEN-LAST:event_txtNombreLibroKeyPressed

    private void txtCategoriaLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCategoriaLibroActionPerformed
        listarLibros(false);
    }//GEN-LAST:event_txtCategoriaLibroActionPerformed

    private void tbRegistrarLibroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbRegistrarLibroMouseClicked
        if (evt.getClickCount() == 2) {
            if (quienLlamaD instanceof PrestamoLibro) {
                int f = tbRegistrarLibro.getSelectedRow();
                int codigoLibro = Integer.parseInt(tbRegistrarLibro.getValueAt(f, 0).toString());
                ((PrestamoLibro) quienLlamaD).obtenerLibro(codigoLibro);
            }
            this.dispose();
        }
    }//GEN-LAST:event_tbRegistrarLibroMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BuscarLibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BuscarLibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BuscarLibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BuscarLibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                BuscarLibro dialog = new BuscarLibro(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnSalir;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton rbTodos;
    private javax.swing.JTable tbRegistrarLibro;
    private javax.swing.JTextField txtAutorLibro;
    private javax.swing.JTextField txtCategoriaLibro;
    private javax.swing.JTextField txtNombreLibro;
    // End of variables declaration//GEN-END:variables
}
