
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

public class PrestamoLibro extends javax.swing.JDialog {
    
    private AddMiembroItf miembroRMI;
    private Map<String, String> parametros;
    private Miembro miembro;
    private AddLibroIntf libroRMI = null;
    private Libro libro;
    private Libro libroSeleccionado;
    private int filaSeleccionada;
    private ArrayList<Integer> codigosLibros = new ArrayList<Integer>();
    private AddPrestamoIntf prestamoRMI;
    private Prestamo prestamo;
    
    public PrestamoLibro(JDialog parent, boolean modal) {
        super(parent, modal);
        initComponents();
        postBuild();
    }
    
    public void postBuild() {
        parametros = Util.leerArchivo("parametros.txt");
        try {
            Registry registry = LocateRegistry.getRegistry(parametros.get("host"), Integer.parseInt(parametros.get("puerto")));
            miembroRMI = (AddMiembroItf) registry.lookup("miembroRMI");
            libroRMI = (AddLibroIntf) registry.lookup("libroRMI");
            prestamoRMI = (AddPrestamoIntf) registry.lookup("prestamoRMI");
        } catch (Exception e) {
            e.printStackTrace();
        }
        btnAgregarLibro.setEnabled(false);
        btnQuitarLibro.setEnabled(false);
        libroSeleccionado = null;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtIdiLibro = new javax.swing.JTextField();
        txtNombreLibro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btnBuscarLibro = new javax.swing.JButton();
        txtIdCliente = new javax.swing.JTextField();
        txtCliente = new javax.swing.JTextField();
        btnBuscarCliente = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbLibro = new javax.swing.JTable();
        btnAgregarLibro = new javax.swing.JButton();
        btnQuitarLibro = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtaObservaciones = new javax.swing.JTextArea();
        btnConfirmar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jcFechaReserva = new com.toedter.calendar.JDateChooser();
        jcFechaDevolucion = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("PRESTAMO DE LIBROS");

        jLabel1.setText("Libro:");

        txtIdiLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdiLibroActionPerformed(evt);
            }
        });

        txtNombreLibro.setEnabled(false);

        jLabel2.setText("Cliente:");

        btnBuscarLibro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar.png"))); // NOI18N
        btnBuscarLibro.setToolTipText("Buscar Libro");
        btnBuscarLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarLibroActionPerformed(evt);
            }
        });

        txtIdCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdClienteActionPerformed(evt);
            }
        });

        txtCliente.setEnabled(false);
        txtCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClienteActionPerformed(evt);
            }
        });

        btnBuscarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar.png"))); // NOI18N
        btnBuscarCliente.setToolTipText("Buscar Cliente");
        btnBuscarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarClienteActionPerformed(evt);
            }
        });

        tbLibro.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre", "Autor", "Categoria"
            }
        ));
        tbLibro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbLibroMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbLibro);
        tbLibro.getColumnModel().getColumn(0).setPreferredWidth(20);
        tbLibro.getColumnModel().getColumn(1).setPreferredWidth(40);

        btnAgregarLibro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo.png"))); // NOI18N
        btnAgregarLibro.setToolTipText("Agregar Libro");
        btnAgregarLibro.setName(""); // NOI18N
        btnAgregarLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarLibroActionPerformed(evt);
            }
        });

        btnQuitarLibro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menos.png"))); // NOI18N
        btnQuitarLibro.setToolTipText("Quitar Libro");
        btnQuitarLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarLibroActionPerformed(evt);
            }
        });

        jLabel3.setText("Fecha de Reserva:");

        jLabel4.setText("Fecha de Devolucion:");

        jLabel5.setText("Observaciones:");

        txtaObservaciones.setColumns(20);
        txtaObservaciones.setRows(5);
        jScrollPane2.setViewportView(txtaObservaciones);

        btnConfirmar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/confirmar.png"))); // NOI18N
        btnConfirmar.setText("CONFIRMAR");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar.png"))); // NOI18N
        btnCancelar.setText("CANCELAR");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel1)
                        .addGap(4, 4, 4)
                        .addComponent(txtIdiLibro, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(txtNombreLibro, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscarLibro)
                        .addGap(31, 31, 31)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(txtIdCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscarCliente))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(btnAgregarLibro, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(btnQuitarLibro, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 666, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jcFechaReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(193, 193, 193)
                        .addComponent(jLabel4)
                        .addGap(7, 7, 7)
                        .addComponent(jcFechaDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel5))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 666, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(200, 200, 200)
                        .addComponent(btnConfirmar, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtIdiLibro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNombreLibro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnBuscarLibro)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtIdCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(btnBuscarCliente)))
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAgregarLibro)
                    .addComponent(btnQuitarLibro))
                .addGap(6, 6, 6)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jcFechaReserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jcFechaDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(jLabel5)
                .addGap(6, 6, 6)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnConfirmar)
                    .addComponent(btnCancelar))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarClienteActionPerformed
        // TODO add your handling code here:
        BuscarMiembro buscarMienbro = new BuscarMiembro(this, true);
        buscarMienbro.setVisible(true);
    }//GEN-LAST:event_btnBuscarClienteActionPerformed
    
    private void txtClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtClienteActionPerformed
    
    private void tbLibroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbLibroMouseClicked
        try {
            filaSeleccionada = tbLibro.getSelectedRow();
            libroSeleccionado = libroRMI.find(Integer.parseInt(tbLibro.getValueAt(filaSeleccionada, 0).toString()));
            btnQuitarLibro.setEnabled(true);
            txtIdiLibro.setText(libroSeleccionado.getId_libro() + "");
            txtNombreLibro.setText(libroSeleccionado.getNombre());
        } catch (RemoteException ex) {
            Logger.getLogger(PrestamoLibro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_tbLibroMouseClicked
    
    private void btnBuscarLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarLibroActionPerformed
        
        BuscarLibro buscarLibro = new BuscarLibro(this, true);
        buscarLibro.setVisible(true);
    }//GEN-LAST:event_btnBuscarLibroActionPerformed
    
    private void btnAgregarLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarLibroActionPerformed
        
        if (libro != null) {
            DefaultTableModel model = (DefaultTableModel) tbLibro.getModel();
            Object[] rowdata = {libro.getId_libro(), libro.getNombre(), libro.getAutor(), libro.getGenero()};
            if (validarAgregarLibro()) {
                model.addRow(rowdata);
            } else {
                JOptionPane.showMessageDialog(this, "No se puede prestar el mismo libro");
                libro = null;
                txtIdiLibro.setText("");
                txtNombreLibro.setText("");
            }
            btnAgregarLibro.setEnabled(false);
        } else {
            JOptionPane.showMessageDialog(this, "No hay un libro para agregar");
        }
        txtIdiLibro.setText("");
        txtNombreLibro.setText("");
        libro = null;
        
    }//GEN-LAST:event_btnAgregarLibroActionPerformed
    
    private void btnQuitarLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarLibroActionPerformed
        DefaultTableModel modelo = (DefaultTableModel) tbLibro.getModel();
        if (libroSeleccionado != null) {
            int response = JOptionPane.showConfirmDialog(null, "¿Estas seguro que deseas quitar el libro?", "Mensaje del Sistema",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (response == JOptionPane.YES_OPTION) {
                modelo.removeRow(filaSeleccionada);
                txtIdiLibro.setText("");
                txtNombreLibro.setText("");
                quitarCodigoLibro(libroSeleccionado.getId_libro());
            }
            
        }
        libroSeleccionado = null;
        btnQuitarLibro.setEnabled(false);
    }//GEN-LAST:event_btnQuitarLibroActionPerformed
    
    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed
        Date fechaDevolucion = jcFechaDevolucion.getDate();
        Date fechaPrestamo = jcFechaReserva.getDate();
        String cliente = txtCliente.getText();
        List<Libro> libros = null;
        DefaultTableModel modelo = (DefaultTableModel) tbLibro.getModel();
        int filas = modelo.getRowCount();
        try {
            if (Util.validarCadenas(txtCliente) && jcFechaDevolucion != null && jcFechaDevolucion != null && filas > 0) {
                prestamo = new Prestamo();
                prestamo.setFecha_devolucion(fechaDevolucion);
                prestamo.setFecha_prestamo(fechaPrestamo);
                prestamo.setId_miembro(miembro.getId_miembro());
                prestamo.setObservaciones(txtaObservaciones.getText());
                libros = obtenerLibrosJTable();
                
                prestamoRMI.relizarPrestamo(prestamo, libros);
                JOptionPane.showMessageDialog(this, "Se realizo el prestamo exitosamente", "mensaje del sistema", JOptionPane.DEFAULT_OPTION);
                limpiarCampos();
                
            } else {
                JOptionPane.showMessageDialog(this, "Hay campos obligatorios vacios", "mensaje del sistema", JOptionPane.WARNING_MESSAGE);
            }
        } catch (RemoteException ex) {
            JOptionPane.showMessageDialog(this, "Hubo un error al realizar el prestamo", "mensaje del sistema", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(PrestamoLibro.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }//GEN-LAST:event_btnConfirmarActionPerformed
    
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        limpiarCampos();
    }//GEN-LAST:event_btnCancelarActionPerformed
    
    private void txtIdiLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdiLibroActionPerformed
        try {
            libro = libroRMI.find(Integer.parseInt(txtIdiLibro.getText()));
            txtIdiLibro.setText(libro.getId_libro() + "");
            txtNombreLibro.setText(libro.getNombre());
            btnAgregarLibro.setEnabled(true);
        } catch (RemoteException ex) {
            Logger.getLogger(PrestamoLibro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_txtIdiLibroActionPerformed
    
    private void txtIdClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdClienteActionPerformed
        // TODO add your handling code here:
        try {
            miembro = miembroRMI.find(Integer.parseInt(txtIdCliente.getText()));
            txtIdCliente.setText(miembro.getId_miembro() + "");
            txtCliente.setText(miembro.getNombre() + " " + miembro.getApellido());
            
            
        } catch (RemoteException ex) {
            Logger.getLogger(PrestamoLibro.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_txtIdClienteActionPerformed
    public void obtenerMiembro(int id) {
        try {
            miembro = miembroRMI.find(id);
            txtIdCliente.setText(miembro.getId_miembro() + "");
            txtCliente.setText(miembro.getNombre() + " " + miembro.getApellido());
            
            
        } catch (RemoteException ex) {
            Logger.getLogger(PrestamoLibro.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public void limpiarCampos() {
        DefaultTableModel modelo = (DefaultTableModel) tbLibro.getModel();
        modelo.setRowCount(0);
        codigosLibros.clear();
        miembro = null;
        libro = null;
        txtCliente.setText("");
        txtIdCliente.setText("");
        txtaObservaciones.setText("");
        jcFechaDevolucion.setCalendar(null);
        jcFechaReserva.setCalendar(null);
        txtaObservaciones.setText("");
    }
    
    public void quitarCodigoLibro(int codigoLibro) {
        
        for (int i = 0; i < codigosLibros.size(); i++) {
            
            if (codigosLibros.get(i) == codigoLibro) {
                codigosLibros.remove(i);
            }
            
        }
    }
    
    public List<Libro> obtenerLibrosJTable() {
        List<Libro> libros = new ArrayList<Libro>();
        Libro libro = null;
        DefaultTableModel modelo = (DefaultTableModel) tbLibro.getModel();
        int filas = modelo.getRowCount();
        for (int i = 0; i < filas; i++) {
            try {
                libro = libroRMI.find(Integer.parseInt(tbLibro.getValueAt(i, 0).toString()));
                libros.add(libro);
            } catch (RemoteException ex) {
                Logger.getLogger(PrestamoLibro.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        return libros;
    }
    
    public boolean validarAgregarLibro() {
        int cont = 0;
        for (Integer codigo : codigosLibros) {
            if (libro.getId_libro() == codigo) {
                cont++;
            }
            if (cont == 2) {
                return false;
                
            }
        }
        return true;
    }
    
    public void obtenerLibro(int id) {
        try {
            libro = libroRMI.find(id);
            txtIdiLibro.setText(libro.getId_libro() + "");
            txtNombreLibro.setText(libro.getNombre());
            btnAgregarLibro.setEnabled(true);
            btnQuitarLibro.setEnabled(false);
            codigosLibros.add(libro.getId_libro());
        } catch (RemoteException ex) {
            Logger.getLogger(PrestamoLibro.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PrestamoLibro.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PrestamoLibro.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PrestamoLibro.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PrestamoLibro.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                PrestamoLibro dialog = new PrestamoLibro(new JDialog(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarLibro;
    private javax.swing.JButton btnBuscarCliente;
    private javax.swing.JButton btnBuscarLibro;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnConfirmar;
    private javax.swing.JButton btnQuitarLibro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private com.toedter.calendar.JDateChooser jcFechaDevolucion;
    private com.toedter.calendar.JDateChooser jcFechaReserva;
    private javax.swing.JTable tbLibro;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtIdCliente;
    private javax.swing.JTextField txtIdiLibro;
    private javax.swing.JTextField txtNombreLibro;
    private javax.swing.JTextArea txtaObservaciones;
    // End of variables declaration//GEN-END:variables
}
