
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.Map;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ricardo
 */
public class test {

    public static void main(String[] args) {
        Map<String, String> parametros = Util.leerArchivo("parametros.txt");
        try {
            Registry registry = LocateRegistry.getRegistry(parametros.get("host"), Integer.parseInt(parametros.get("puerto")));
            AddMiembroItf miembroRMI = (AddMiembroItf) registry.lookup("miembroRMI");
            AddLibroIntf libroRMI = (AddLibroIntf) registry.lookup("libroRMI");
            Libro l = libroRMI.find(2);
            System.out.println("l" + l.getNombre());
            List<Miembro> miembros = miembroRMI.findAll();
            for(Miembro m : miembros){
                System.out.println("m" + m.getNombre());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
